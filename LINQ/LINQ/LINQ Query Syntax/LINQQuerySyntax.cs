﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LINQ.Model;

namespace LINQ.LINQ_Query_Syntax
{
    class LINQQuerySyntax
    {
        static void LinqDqurySyn()
        {
            // string collection
            IList<Student> studentList = new List<Student>()
            {
                new Student() {StudentID = 1,StudentName = "Ismail", Age = 32},
                new Student() {StudentID = 2,StudentName = "Tuhin", Age = 32},
                new Student() {StudentID = 3,StudentName = "Khosru", Age = 25},
                new Student() {StudentID = 4,StudentName = "Nuruddin", Age = 27},
                new Student() {StudentID = 5,StudentName = "Touhid", Age = 28},
            };

            // LINQ Query Syntax
            var result = from s in studentList
                where s.Age >= 25 && s.Age < 30
                select s;
            Console.WriteLine("Teen Age Boys Are................... ");
            foreach (Student std in result)
            {
                Console.WriteLine(std.StudentName);
            }
            Console.ReadKey();
        }
    }
}
